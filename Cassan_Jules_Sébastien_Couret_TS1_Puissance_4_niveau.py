##################
# Couret Sébastien  #
# Cassan Jules TS1 #
#### Niveau 3#######

import sys # Ici nous importons un module pour quitter le programme si l'utilisateur gagne

tableau=[]
tableauv=[]
tableauv2=[]
qst1=x1=x2=qst2=0


for j in range(0,6):
    tableau.append([])    #Création des variables et du tableau qui est composé de 6 colonnes et 7 rangées
    for i in range(0,7):
        tableau[j].append(0)

for j in range(0,6):
    tableauv.append([])    #Ici nous créeons deux tableaux qui nous permettrons de mettre fin au jeu si la grille se remplit entièrement en ajoutant une valeur à l'emplacement du joueur et enregardant si la grille est pleine
    for i in range(0,7):
        tableauv[j].append(0)

for j in range(0,6):
    tableauv2.append([])    
    for i in range(0,7):
        tableauv2[j].append("p")

def afftab(tableau): # Fonction pour afficher le tableau
    print("",tableau[5],"\n",tableau[4],"\n",tableau[3],"\n",tableau[2],"\n",tableau[1],"\n",tableau[0])

def j1(x1,qst1): # Fonction pour le joueur 1 avec x1 qui correspond à la colonne sélectionnée
    qst1=input("\nDans quelle colonne voulez vous mettre votre pion rouge  ?")
    x1=int(qst1)
    l1=ligne(tableau,x1)
    tableau[l1][x1-1]='R'  #Comme expliqué precedemment , les deux tableaux se remplissent uniformement
    tableauv[l1][x1-1]='p'
    
def j2(x2,qst2): # De même pour le joueur 2 , nous remplissons également notre second tableau qui sert à mettre fin au jeu
    qst2=input("\nDans quelle colonne voulez vous mettre votre pion jaune ?")
    x2=int (qst2)
    l2=ligne(tableau,x2)
    tableau[l2][x2-1]='J'
    tableauv[l2][x2-1]='p'

def ligne(tableau,x1): #Notre fonction se balade dans les différentes lignes afin de positionner le pion à la bonne place et de trouver la bonne ligne ou placer le pion
    ligne=-1
    for i in range(0,6):
        if tableau[i][x1-1]==0:
            ligne=i
            return ligne

def ligne(tableau,x2): # Encore une fois mais pour le joueur deux
    ligne=-1
    for i in range(0,6):
        if tableau[i][x2-1]==0:
            ligne=i
            return ligne


# A partir d'ici ,nous créeons toutes les combinaisons possibles ,d'abord horizontales puis verticales pour chaque joueur 
def victhorizj1(tableau):
    x1=int(qst1)   #Nos fonctions se baladent dans les lignes afin de vérifier si 4 jetons sont alignés
    for i in range(0,6):
        if tableau[i][x1]=="R" and tableau[i][x1+1]=="R" and tableau[i][x1+2]=="R" and tableau[i][x1+3]=="R"  or  tableau[i][x1+1]=="R" and tableau[i][x1+2]=="R" and tableau[i][x1+3]=="R" and tableau[i][x1+4]=="R" or  tableau[i][x1+2]=="R" and tableau[i][x1+3]=="R" and tableau[i][x1+4]=="R" and tableau[i][x1+5]=="R"  :
            print("\nLe joueur 1 a gagné !!!")
            sys.exit(0)

def victhorizj2(tableau):
    x2=int(qst2)
    for i in range(0,6):
        if tableau[i][x2]=="J" and tableau[i][x2+1]=="J" and tableau[i][x2+2]=="J" and tableau[i][x2+3]=="J"  or  tableau[i][x2+1]=="J" and tableau[i][x2+2]=="J" and tableau[i][x2+3]=="J" and tableau[i][x2+4]=="J" or  tableau[i][x2+2]=="J" and tableau[i][x2+3]=="J" and tableau[i][x2+4]=="J" and tableau[i][x2+5]=="J"  :
            print("\nLe joueur 2 a gagné !!!")
            sys.exit(0)

def victvertj1(tableau):
    for i in range(0,6):
        if tableau[5][i]=="R" and  tableau[4][i]=="R" and tableau[3][i]=="R" and tableau[2][i]=="R" or tableau[4][i]=="R" and  tableau[3][i]=="R" and tableau[2][i]=="R" and tableau[1][i]=="R" or tableau[3][i]=="R" and  tableau[2][i]=="R" and tableau[1][i]=="R" and tableau[0][i]=="R":
            print("\n Le joueur 1 a gagné")
            sys.exit(0)
            

def victvertj2(tableau):
    for i in range(0,6):
        if tableau[5][i]=="J" and  tableau[4][i]=="J" and tableau[3][i]=="J" and tableau[2][i]=="J" or tableau[4][i]=="J" and  tableau[3][i]=="J" and tableau[2][i]=="J" and tableau[1][i]=="J" or tableau[3][i]=="J" and  tableau[2][i]=="J" and tableau[1][i]=="J" and tableau[0][i]=="J":
            print("\n Le joueur 2 a gagné")
            sys.exit(0)



#-------------------------------------------------------------------------------------------------------Programme principal--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Il ne nous reste plus qu'à appeller nos fonctions
qst=input("Voulez vous jouer ?(oui/non)")
if qst=="oui":
    while tableauv!=tableauv2:
        afftab(tableau)
        j1(x1,qst1)
        victhorizj1(tableau)
        victvertj1(tableau)
        afftab(tableau)
        j2(x2,qst2)
        victhorizj2(tableau)
        victvertj2(tableau)
        




